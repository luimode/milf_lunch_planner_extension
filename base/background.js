(() => {
    /**** CONSTANTS ****/

    const VOTES_URL = 'https://mvp8nxdedb.execute-api.eu-west-1.amazonaws.com/prod/votes';
    const OPTIONS_URL = 'https://mvp8nxdedb.execute-api.eu-west-1.amazonaws.com/prod/options';
    // const VOTES_URL = 'http://localhost:8081/votes';
    // const OPTIONS_URL = 'http://localhost:8081/options';
    const API_KEY = '8hlsdPZ3VSas025tgQkUO7HBzpsxWEI952xBrxm2'; // a public api key, requests throttling is done by the API GW

    const headers = {
        'Accept': 'application/json; charset=utf-8',
        'Content-Type': 'application/json; charset=utf-8',
        'x-api-key': API_KEY,
    };

    /**** REQUESTS ****/

    const sendVoteRequest = (payload) => {
        ajaxPost(VOTES_URL, payload,
            (response) => console.log("sendVoteRequest::response:" + JSON.stringify(response)),
            (err) => {
                console.error('sendVoteRequest::error:' + JSON.stringify(err));
                sendChromeMessage('request_error', err);
            },
            () => sendChromeMessage('vote_submitted'),
        )
    };

    const sendOptionSubmitRequest = (payload) => {
        ajaxPost(OPTIONS_URL, payload,
            (response) => console.log("sendOptionSubmitRequest::response:" + JSON.stringify(response)),
            (err) => {
                console.error('sendOptionSubmitRequest::error:' + JSON.stringify(err));
                sendChromeMessage('request_error', err);
            },
            () => sendChromeMessage('option_submitted')
        )
    };

    const fetchVotes = () => {
        ajaxGet(
            VOTES_URL,
            (response) => sendChromeMessage('votes_fetched', response),
            (err) => {
                console.log(`error on ${VOTES_URL}:` + JSON.stringify(err));
                sendChromeMessage('request_error', err);
            });
    };

    const ajaxPost = (url, data, onResponse, onFail, always) => {
        $.ajax({
            url,
            type: 'POST',
            headers,
            data: JSON.stringify(data),
            dataType: "json"
        })
            .done(onResponse)
            .fail(onFail)
            .always(always);
    };

    const ajaxGet = (url, onResponse, onFail) => {
        $.ajax({
            url,
            headers,
            type: 'GET',
        })
            .done(onResponse)
            .fail(onFail);
    };

    sendChromeMessage = (type, payload) => {
        chrome.runtime.sendMessage({type, payload});
    };

    const configurePageMatcher = () => {
        chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
            chrome.declarativeContent.onPageChanged.addRules([{
                conditions: [
                    new chrome.declarativeContent.PageStateMatcher({pageUrl: {urlMatches: '.*'}}),
                ],
                actions: [new chrome.declarativeContent.ShowPageAction()],
            }]);
        });
    };

    addAlarmListener = () => {
        chrome.alarms.onAlarm.addListener(alarm => {
            chrome.notifications.create('notification', {
                type: 'basic',
                iconUrl: 'src/images/lunch-icon_128.png',
                title: 'It\'s Lunch Time!',
                message: 'Click on the extension button to vote',
                priority: 1,
            }, notificationId => {
            });
        });
    };

    chrome.runtime.onInstalled.addListener(() => {
        configurePageMatcher();
        addAlarmListener();
    });

    const messageToHandlerMap = {
        'submit_vote' : message => sendVoteRequest(message.payload),
        'refresh_data': message => fetchVotes(),
        'submit_option': message => sendOptionSubmitRequest(message.payload),
        'open_link': message => chrome.tabs.create({url: message.payload, active: false}),
    };

    const addListeners = () => {
            chrome.runtime.onMessage.addListener(message => {
                return messageToHandlerMap[message.type] ?
                    messageToHandlerMap[message.type](message) :
                    console.log('error::unknown message type:' + message.type);
            })
    };

    addListeners();
})();
