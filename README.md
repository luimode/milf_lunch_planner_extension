## Millenium Falke Lunch Planner - Chrome Extension

A voting chrome extension for the ex-MilFs. Supporting daily lunch since 2019.

### Installation:

    1. $ cd popup && npm install && npm run build
    2. Visit chrome://extensions (via omnibox or menu -> Tools -> Extensions).
    3. Enable Developer mode by ticking the checkbox in the upper-right corner.
    4. Click on the "Load unpacked extension..." button.
    4. Select the 'dist' directory that was created after you run npm run build
    
### Update:

    1. Pull the latest version
    2. Go to chrome://extensions/
    3. click on the "reload" button of the MILF Lunch Planner
    
### Development: 

A friendly starting point is the [Chrome extensions documentation](https://developer.chrome.com/extensions/getstarted#unpacked)

### ChangeLog: 

##### Version 1.0:

    * voting is possible for a fixed list of options
    * voter's name is used as voterID
    * polls and displays voting results

##### Version 1.1: 

    * voting is allowed between 10:00 and 12:00
    * uses a voter-secret to identify a voter (replaces the voter's name)
    * winning option(s) appear at the top
    * allow unicode names (VIP feature for Björn)
    
##### Version 1.2: 

    * added multiple views - Vote & Add
    * voter's can add lunch options
    * migrated from jquery to React-redux and material-ui
