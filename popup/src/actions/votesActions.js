
export const FETCH_VOTES_SUCCESS_ACTION = 'FETCH_VOTES_SUCCESS_ACTION ';
export const fetchVotesSuccessAction = data => {
    return {
        type: FETCH_VOTES_SUCCESS_ACTION,
        payload: data,
    };
};

export const IS_SUBMIT_PENDING = 'IS_SUBMIT_PENDING ';
export const isSubmitPendingAction = isPending => {
    return {
        type: IS_SUBMIT_PENDING,
        payload: isPending,
    };
};

export const IS_OPTION_SUBMIT_PENDING = 'IS_OPTION_SUBMIT_PENDING ';
export const isOptionSubmitPendingAction = isPending => {
    return {
        type: IS_OPTION_SUBMIT_PENDING,
        payload: isPending,
    };
};

