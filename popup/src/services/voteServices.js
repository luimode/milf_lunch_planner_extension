import {mountedStore as store} from '../index';
import {
    fetchVotesSuccessAction,
    isSubmitPendingAction,
    isOptionSubmitPendingAction,
} from '../actions/votesActions';
import {sendChromeMessage, registerMessageListeners} from './chromeServices';
import {
    REFRESH_DATA,
    VOTES_FETCHED,
    REQUEST_ERROR,
    SUBMIT_VOTE,
    SUBMIT_OPTION,
    OPTION_SUBMITTED,
    VOTE_SUBMITTED,
    OPEN_LINK,
} from '../constants/messageTypes';

export const fetchVotes = () => {
    store.dispatch(isSubmitPendingAction(true));
    sendFetchVotesMessage();
    setInterval(() => {
        sendFetchVotesMessage();
    }, 3000);
};

export const sendVote = vote => {
    store.dispatch(isSubmitPendingAction(true));
    sendSubmitVoteMessage(vote);
};

export const sendOption = optionObj => {
    store.dispatch(isOptionSubmitPendingAction(true));
    sendChromeMessage(SUBMIT_OPTION, optionObj);
};

const sendFetchVotesMessage = () => {
    sendChromeMessage(REFRESH_DATA);
};

const sendSubmitVoteMessage = vote => {
    sendChromeMessage(SUBMIT_VOTE, vote);
};

export const openLink = url => {
    if (url) {
        sendChromeMessage(OPEN_LINK, url);
    }
};

let inited = false;
const messageToHandlerMap = {
    [VOTES_FETCHED]: msg => {
        store.dispatch(fetchVotesSuccessAction(msg.payload));
        if (!inited) {
            store.dispatch(isSubmitPendingAction(false));
            inited = true;
        }
    },
    [VOTE_SUBMITTED]: () => store.dispatch(isSubmitPendingAction(false)),
    [OPTION_SUBMITTED]: () => store.dispatch(isOptionSubmitPendingAction(false)),
    [REQUEST_ERROR]: msg => {
        console.error(`fetchError::${JSON.stringify(msg.payload)}`);
        store.dispatch(isSubmitPendingAction(false));
    },
};

(() => {
    registerMessageListeners(messageToHandlerMap);
})();
