import {getChromeMockEnv} from './mockChromeEnv';

const isDevEnv = () => {
    return process.env.NODE_ENV === 'development';
};

const getChromeRef = () => {
    return isDevEnv() ? getChromeMockEnv() : chrome;
};

export const getFromChromeStorage = (keysArray, onResult) => {
    const {storage} = getChromeRef();
    storage.sync.get(keysArray, onResult);
};

export const setToChromeStorage = payload => {
    const {storage} = getChromeRef();
    storage.sync.set(payload);
};

export const sendChromeMessage = (type, payload) => {
    const {runtime} = getChromeRef();
    runtime.sendMessage(runtime.id, {type, payload});
};

export const registerMessageListeners = messageToHandlerMap => {
    const {runtime} = getChromeRef();
    runtime.onMessage.addListener(
        msg => messageToHandlerMap[msg.type] ?
            messageToHandlerMap[msg.type](msg) :
            console.log(`addListener::unknown message type::${msg.type}`)
    );
};
