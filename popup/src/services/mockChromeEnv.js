// mocks the Chrome env for testing outside Chrome with hot reloading
export const getChromeMockEnv = () => {
    return {
        storage: {
            sync: {
                get: (keysArray, onResult) =>
                    console.log('chrome.storage.sync.get() called with params keysArray:' + keysArray + ", onResult:" + onResult),
            },
        },
        runtime: {
            id: 1,
            sendMessage: (runtimeId, msg) =>
                console.log('chrome.runtime.sendMessage() called with params keysArray:' + runtimeId + ", msg:" + msg),
            onMessage: {
                addListener: (listener) => {
                    console.log('chrome.runtime.onMessage.addListener() called with params listener:' + listener);
                },
            },
        },
    };
};
