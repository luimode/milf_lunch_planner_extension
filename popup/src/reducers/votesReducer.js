import {FETCH_VOTES_SUCCESS_ACTION, IS_SUBMIT_PENDING, IS_OPTION_SUBMIT_PENDING} from '../actions/votesActions';

export const initialState = {
    items: [],
    isSubmitPending: false,
};

function handleFetchVotesSuccessAction(state, payload) {
    const newState = {...state};

    newState.items = payload.items;

    return newState;
}

function handleSubmitPendingAction(state, isPending) {
    const newState = {...state};

    newState.isSubmitPending = isPending;

    return newState;
}

function handleSubmitOptionPendingAction(state, isPending) {
    const newState = {...state};

    newState.isSubmitOptionPending = isPending;

    return newState;
}

export default function votesReducer(state = initialState, action = {}) {
    switch (action.type) {
        case FETCH_VOTES_SUCCESS_ACTION:
            return handleFetchVotesSuccessAction(state, action.payload);
        case IS_SUBMIT_PENDING:
            return handleSubmitPendingAction(state, action.payload);
        case IS_OPTION_SUBMIT_PENDING:
            return handleSubmitOptionPendingAction(state, action.payload);
        default:
            return {...state};
    }
}
