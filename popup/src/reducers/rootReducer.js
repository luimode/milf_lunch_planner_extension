import {combineReducers} from 'redux';
import votesReducer from './votesReducer';

const appReducer = combineReducers({
    votes: votesReducer,
});

export default (state, action) => {
    return appReducer(state, action);
};
