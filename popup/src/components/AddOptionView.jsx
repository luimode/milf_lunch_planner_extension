import React, {Component} from 'react';
import {hot} from 'react-hot-loader/root';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import {sendOption} from '../services/voteServices';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {getFromChromeStorage, setToChromeStorage} from '../services/chromeServices';
import {STORAGE_KEY_OPTION_NAME, STORAGE_KEY_OPTION_URL, STORAGE_KEY_VOTER_ID} from '../constants/storageKeyNames';

// todo move to constants
const URL_REGEX = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;

class AddOptionView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            optionValue: '',
            optionNameValid: true,
            urlValue: '',
            urlValueValid: true,
        };

        this.handleOptionInputChange = this.handleOptionInputChange.bind(this);
        this.handleUrlInputChange = this.handleUrlInputChange.bind(this);
        this.onOptionSubmitClick = this.onOptionSubmitClick.bind(this);
        this.synchFields = this.synchFields.bind(this);
    }

    componentDidMount() {
        this.synchFields();
    }

    synchFields() {
        getFromChromeStorage([STORAGE_KEY_OPTION_NAME, STORAGE_KEY_OPTION_URL], result => {
            if (result[STORAGE_KEY_OPTION_NAME]) {
                this.setState({optionValue: result[STORAGE_KEY_OPTION_NAME]});
                this.setState({optionNameValid: true});
            }

            if (result[STORAGE_KEY_OPTION_URL]) {
                this.setState({urlValue: result[STORAGE_KEY_OPTION_URL]});
                this.setState({urlValueValid: true});
            }
        });
    }

    handleOptionInputChange(e) {
        const val = e.target.value;
        this.setState({optionValue: val});

        // todo move to constants
        const isValid = /^[a-zA-Z&-]{3,20}$/.test(val);
        if (isValid || !val) {
            setToChromeStorage({STORAGE_KEY_OPTION_NAME, val});
        }
        this.setState({optionNameValid: isValid});
    }

    handleUrlInputChange(e) {
        const val = e.target.value;
        this.setState({urlValue: val});

        const isValid = URL_REGEX.test(val) || val.length < 1;
        if (isValid) {
            setToChromeStorage({STORAGE_KEY_OPTION_URL, val});
        }
        this.setState({urlValueValid: isValid});
    }

    getOptionFieldError() {
        return !this.state.optionNameValid && 'Invalid! Expected: ^[a-zA-Z&-]{3,20}$';
    }

    getUrlFieldError() {
        return (!this.state.urlValueValid && this.state.urlValue.length > 0) && 'Invalid URL!';
    }

    isOptionButtonDisabled() {
        return !this.state.optionNameValid || !this.state.urlValueValid;
    }

    onOptionSubmitClick() {

        const urlValueValid = URL_REGEX.test(this.state.urlValue) || this.state.urlValue.length < 1;
        // todo move to constants
        const optionNameValid = /^[a-zA-Z&-]{3,20}$/.test(this.state.optionValue);

        this.setState({urlValueValid});
        this.setState({optionNameValid});

        if (urlValueValid && optionNameValid) {
            getFromChromeStorage(STORAGE_KEY_VOTER_ID, result => {
                const optionObj = {
                    voterId: result[STORAGE_KEY_VOTER_ID],
                    optionName: this.state.optionValue,
                    url: this.state.urlValue ? this.state.urlValue : undefined,
                };
                sendOption(optionObj);
            });
        }
    }

    renderSubmitOptionButton() {
        const {isSubmitOptionPending, classes} = this.props;

        return isSubmitOptionPending ?
            <CircularProgress className={classes.progress}/> :
            <Button
                href='#'
                className={classes.submitButton}
                size='large'
                variant='contained'
                color='primary'
                onClick={this.onOptionSubmitClick}
                disabled={this.isOptionButtonDisabled()}
            >
                Fertsch
            </Button>;
    }

    render() {
        const {classes} = this.props;
        return (
            <Typography component='div'>
                <div className={classes.paper}>
                    <TextField
                        required
                        id='optionName'
                        label='Lunch Option Name'
                        style={{width: 250}}
                        margin='normal'
                        value={this.state.optionValue}
                        onChange={this.handleOptionInputChange}
                        error={!this.state.optionNameValid}
                        helperText={this.getOptionFieldError()}
                    />
                    <TextField
                        id='optionUrl'
                        label='Menu URL'
                        style={{width: 250}}
                        margin='dense'
                        value={this.state.urlValue}
                        onChange={this.handleUrlInputChange}
                        error={!this.state.urlValueValid}
                        helperText={this.getUrlFieldError()}
                    />
                    {this.renderSubmitOptionButton()}
                </div>
            </Typography>
        );
    }
}

AddOptionView.propTypes = {
    isSubmitOptionPending: PropTypes.bool.isRequired,
};

const styles = {};

export default hot(withStyles(styles)(AddOptionView));
