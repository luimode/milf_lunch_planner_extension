/*global chrome*/

import React, {Component} from 'react';
import {hot} from 'react-hot-loader/root';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/styles';

const styles = {};

class VoteOption extends Component {
    constructor(props) {
        super(props);
        this.state = {
            optionId: '',
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.synchSelection = this.synchSelection.bind(this);
    }

    componentDidMount() {
        this.synchSelection();
    }

    synchSelection() {

        const {optionId} = this.props;

        chrome.storage && chrome.storage.sync.get([optionId], result => {
            if (result[optionId]) {
                const storedVal = result[optionId];
                this.setState({selected: storedVal});
            }
        });
    }

    handleInputChange(input) {
        const {optionId} = this.props;
        const value = input.target.value;
        const toSave = {};
        toSave[optionId] = value;
        chrome.storage.sync.set(toSave, () => {});
        this.setState({selected: value});

        this.props.onSelectionChanged(value);
    }

    render() {
        const {classes} = this.props;

        return (
            <div></div>
        );
    }
}

VoteOption.propTypes = {
    onSelectionChanged: PropTypes.func.isRequired,

};

export default hot(withStyles(styles)(VoteOption));
