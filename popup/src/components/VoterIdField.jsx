/*global chrome*/
import React, {Component} from 'react';
import {hot} from 'react-hot-loader/root';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/styles';
import TextField from '@material-ui/core/TextField';
import {getFromChromeStorage, setToChromeStorage} from '../services/chromeServices';

import {fingerprints} from '../constants/fingerprints';
import {STORAGE_KEY_VOTER_ID} from '../constants/storageKeyNames';

const styles = {
    inputField: {
        width: '95%',
        marginTop: 20,
    },
    voterField: {
        fontSize: 12,
        textAlign: 'center',
        color: 'gray',
    },
};

class VoterIdField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            voterId: '',
            isValid: false,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.synchVoterId = this.synchVoterId.bind(this);
        this.isValidVoteId = this.isValidVoteId.bind(this);
    }

    componentDidMount() {
        this.synchVoterId();
    }

    synchVoterId() {
        getFromChromeStorage([STORAGE_KEY_VOTER_ID], result =>
            this.setVoterIdToStateAndParent(result[STORAGE_KEY_VOTER_ID]));
    }

    async isValidVoteId(voteId) {
        const digest = await crypto.subtle.digest('SHA-256', new TextEncoder().encode(voteId));
        const hashArray = Array.from(new Uint8Array(digest));
        const hashHex = hashArray.map(b => ('00' + b.toString(16)).slice(-2)).join('');
        return fingerprints.includes(hashHex);
    }

    async handleInputChange(input) {
        const voterId = input.target.value;
        await this.setVoterIdToStateAndParent(voterId);
    }

    async setVoterIdToStateAndParent(voterId) {
        this.setState({voterId});
        const isValid = await this.isValidVoteId(voterId);
        this.setState({isValid});
        if (isValid) {
            // not storing invalid voterIds - if the user removes it by mistake, it can be recovered by reloading the UI
            setToChromeStorage({[STORAGE_KEY_VOTER_ID]: voterId});
        }
        const voterIdForParent = isValid ? voterId : '';
        this.props.onVoterIdSave(voterIdForParent);
    }

    getErrorText() {
        return !this.state.isValid && 'Unknown VoterID';
    }

    render() {
        const {classes} = this.props;

        return (
            <TextField
                id='voterIdField'
                label='Voter ID'
                className={classes.inputField}
                variant='standard'
                placeholder='Your voter ID'
                value={this.state.voterId}
                onChange={this.handleInputChange}
                margin='dense'
                error={!this.state.isValid}
                helperText={this.getErrorText()}
                InputProps={{
                    classes: {
                        input: classes.voterField,
                    },
                }}
            />
        );
    }
}

VoterIdField.propTypes = {
    onVoterIdSave: PropTypes.func.isRequired,
};

export default hot(withStyles(styles)(VoterIdField));
