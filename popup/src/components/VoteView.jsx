import React, {Component} from 'react';
import {hot} from 'react-hot-loader/root';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/styles';
import Typography from '@material-ui/core/Typography';
import VoterIdField from './VoterIdField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import {fetchVotes, openLink, sendVote} from '../services/voteServices';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import {getFromChromeStorage} from '../services/chromeServices';
import {STORAGE_KEY_VOTER_ID} from '../constants/storageKeyNames';

class VoteView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            voterId: '',
            checked: {},
        };

        this.onVoterIdSave = this.onVoterIdSave.bind(this);
        this.renderVoteItems = this.renderVoteItems.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.isSubmitBtnDisabled = this.isSubmitBtnDisabled.bind(this);
        this.handleCheckboxChecked = this.handleCheckboxChecked.bind(this);
    }

    componentDidMount() {
        fetchVotes();
    }

    isSubmitBtnDisabled() {
        return !this.state.voterId || this.getCheckedOptions().length < 1;
    }

    onVoterIdSave(voterId) {
        this.setState({voterId});
    }

    renderVoteItems() {
        const {votes} = this.props;

        return votes && votes
            .sort((v1, v2) => v2.votes.length - v1.votes.length)
            .map(vote =>
                // todo move to functional component
                <div>
                    <ListItem button key={vote.name} alignItems='flex-start' onClick={() => openLink(vote.url)}>
                        <ListItemText
                            primary={vote.name}
                            secondary={this.stringifyVoters(vote.votes)}
                        />
                        <ListItemSecondaryAction>
                            <Checkbox
                                color='primary'
                                edge='end'
                                onChange={(event, checked) =>
                                    this.handleCheckboxChecked(event, checked, vote.name)}
                                checked={this.state.checked[vote.name]}
                            />
                        </ListItemSecondaryAction>
                    </ListItem>
                    <Divider variant='middle' component='li'/>
                </div>
            );
    }

    handleCheckboxChecked(event, isCheckboxChecked, voteName) {
        const checked = {...this.state.checked};
        checked[voteName] = isCheckboxChecked;
        this.setState({checked});
    }

    stringifyVoters(voters) {
        let res = '';
        voters.forEach((voterName, ix) => {
            res += voterName.charAt(0).toUpperCase() + voterName.slice(1);
            if (ix < voters.length - 1) {
                res += ', ';
            }
        });
        return res;
    }

    renderAddOptionButton() {
        const {classes, onAddButtonClick} = this.props;
        return (
            (this.props.votes && this.props.votes.length > 0) &&
            <Fab
                color='primary'
                aria-label='Add'
                size='small'
                className={classes.fab}
                onClick={onAddButtonClick}
            >
                <AddIcon/>
            </Fab>
        );
    }

    renderSubmitButton() {
        const {isSubmitPending, classes} = this.props;

        return isSubmitPending ?
            <CircularProgress className={classes.progress}/> :
            <Button
                href='#'
                className={classes.submitButton}
                size='large'
                variant='contained'
                color='primary'
                onClick={this.onSubmit}
                disabled={this.isSubmitBtnDisabled()}
            >
                Fertsch
            </Button>;
    }

    onSubmit() {
        getFromChromeStorage(STORAGE_KEY_VOTER_ID, result => {
            const vote = {
                voterId: result[STORAGE_KEY_VOTER_ID],
                votes: this.getCheckedOptions(),
            };
            sendVote(vote);
        });
    }

    getCheckedOptions() {
        const checked = this.state.checked;
        const checkedOptions = [];
        for (const option in checked) {
            if (checked[option]) {
                checkedOptions.push(option);
            }
        }
        return checkedOptions;
    }

    render() {
        const {classes} = this.props;
        return (
            <Typography component='div'>
                <div className={classes.paper}>
                    <VoterIdField classes={classes}
                                  onVoterIdSave={this.onVoterIdSave}
                    />
                    <List className={classes.list}>
                        {this.renderVoteItems()}
                    </List>
                    {this.renderAddOptionButton()}
                    {this.renderSubmitButton()}
                </div>
            </Typography>
        );
    }
}

VoteView.propTypes = {
    onAddButtonClick: PropTypes.func.isRequired,
    votes: PropTypes.object.isRequired,
    isSubmitPending: PropTypes.bool.isRequired,
};

const styles = {};

export default hot(withStyles(styles)(VoteView));
