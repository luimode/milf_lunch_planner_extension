export const REFRESH_DATA = 'refresh_data';
export const VOTES_FETCHED = 'votes_fetched';
export const REQUEST_ERROR = 'request_error';
export const SUBMIT_VOTE = 'submit_vote';
export const VOTE_SUBMITTED = 'vote_submitted';
export const SUBMIT_OPTION = 'submit_option';
export const OPTION_SUBMITTED = 'option_submitted';
export const OPEN_LINK = 'open_link';

