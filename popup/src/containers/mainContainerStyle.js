export const styles = {
    '@global': {
        body: {
            backgroundColor: 'white',
            width: 375,
            height: 350,
        },
        '*::-webkit-scrollbar': {
            width: '0.4em',
            backgroundColor: '#F5F5F5',
        },
        '*::-webkit-scrollbar-track': {
            '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.3)',
            borderRadius: '10px',
            backgroundColor: '#F5F5F5',
        },
        '*::-webkit-scrollbar-thumb': {
            borderRadius: '10px',
            backgroundColor: '#9c9c9c',
            outline: '1px solid slategrey',
            '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,.3)',
        },
    },
    root: {
        paddingTop: 10,
        paddingRight: 15,
        paddingLeft: 15,
    },
    paper: {
        marginTop: '20',
        marginBottom: 15,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    list: {
        width: '99%',
        maxWidth: 360,
        backgroundColor: 'white',
        position: 'relative',
        overflow: 'auto',
        maxHeight: 300,
    },
    submitButton: {
        marginBottom: 5,
        marginTop: 25,
        width: 200,
    },
    progress: {
        marginBottom: 5,
        marginTop: 25,
    },
    fab: {
        position: 'absolute',
        bottom: 55,
        right: 12,
    },
};
