import React, {Component} from 'react';
import {hot} from 'react-hot-loader/root';
import {connect} from 'react-redux';
import {withStyles, createMuiTheme} from '@material-ui/core/styles';
import {ThemeProvider} from '@material-ui/styles';
import blue from '@material-ui/core/colors/blue';
import CssBaseline from '@material-ui/core/CssBaseline';
import {Container} from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';
import {styles} from './mainContainerStyle';
import VoteView from '../components/VoteView';
import AddOptionView from '../components/AddOptionView';

const theme = createMuiTheme({
    palette: {
        primary: blue,
    },
});

class MainContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabIndex: 0,
        };

        this.handleTabChange = this.handleTabChange.bind(this);
    }

    handleTabIndexChange(newIndex) {
        this.setState({tabIndex: newIndex});
    }

    handleTabChange(event, newValue) {
        this.handleTabIndexChange(newValue);
    }

    render() {
        const {classes} = this.props;
        return (
            <ThemeProvider theme={theme}>
                <Container component='main' maxWidth='xs' className={classes.root}>
                    <CssBaseline/>

                    <AppBar position='static' color='default'>
                        <Tabs
                            value={this.state.tabIndex}
                            onChange={this.handleTabChange}
                            indicatorColor='primary'
                            textColor='primary'
                            variant='fullWidth'
                        >
                            <Tab label='Vote'/>
                            <Tab label='Add'/>
                        </Tabs>
                    </AppBar>
                    <SwipeableViews
                        axis={'x'}
                        index={this.state.tabIndex}
                        onChangeIndex={this.handleTabIndexChange}
                    >
                        <VoteView
                            onAddButtonClick={() => this.handleTabIndexChange(1)}
                            classes={classes}
                            isSubmitPending={this.props.isSubmitPending}
                            votes={this.props.votes}
                        />
                        <AddOptionView
                            classes={classes}
                            isSubmitOptionPending={this.props.isSubmitOptionPending}
                        />
                    </SwipeableViews>
                </Container>
            </ThemeProvider>
        );
    }
}

MainContainer.propTypes = {};

function mapStateToProps(state) {
    return {
        votes: state.votes.items,
        isSubmitPending: state.votes.isSubmitPending,
        isSubmitOptionPending: state.votes.isSubmitOptionPending,
    };
}

function mapDispatchToProps() {
    return {};
}

export default hot(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MainContainer)));
