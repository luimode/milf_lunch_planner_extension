import React from 'react';
import ReactDOM from 'react-dom';

import {compose, createStore} from 'redux';
import {Provider} from 'react-redux';
import './style/main.css';
import rootReducer from './reducers/rootReducer';

/**************************/
/* Initialize middlewares */
/**************************/

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const createStoreWithMiddleware = composeEnhancers()(createStore);
const mountedStore = createStoreWithMiddleware(rootReducer);

import MainContainer from './containers/MainContainer.jsx';

const RootComponent = () => {
    return (
        <Provider store={mountedStore}>
            <MainContainer />
        </Provider>
    );
};

ReactDOM.render(<RootComponent />, document.getElementById('app'));

export {mountedStore};
