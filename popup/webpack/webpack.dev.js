const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');

const commonPaths = require('./paths');

module.exports = {
    mode: 'development',
    output: {
        filename: `${commonPaths.jsFolder}/[name].js`,
        path: commonPaths.outputPath,
    },
    devtool: 'cheap-module-source-map',
    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true,
                            modules: true,
                            camelCase: true,
                            localIdentName: '[local]___[hash:base64:5]',
                        },
                    },
                    'sass-loader',
                ],
            },
        ],
    },
    devServer: {
        contentBase: commonPaths.outputPath,
        compress: true,
        hot: true,
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new Dotenv({
            path: './.env-dev',
        }),
    ],
};
