const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const CopyPlugin = require('copy-webpack-plugin');

const commonPaths = require('./paths');

module.exports = {
    mode: 'production',
    output: {
        filename: `${commonPaths.jsFolder}/[name].js`, // [name].[hash] omitted as we only file-access the bundle
        path: commonPaths.outputPath,
    },
    optimization: {
        minimizer: [
            new TerserPlugin({
                // Use multi-process parallel running to improve the build speed
                // Default number of concurrent runs: os.cpus().length - 1
                parallel: true,
                cache: true,
                sourceMap: true,
            }),
            new OptimizeCSSAssetsPlugin(),
        ],
    },

    module: {
        rules: [
            {
                test: /\.(css|scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: false,
                            modules: true,
                            camelCase: true,
                            localIdentName: '[local]___[hash:base64:5]',
                        },
                    },
                    'sass-loader',
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: `${commonPaths.cssFolder}/[name].css`,
            chunkFilename: `${commonPaths.cssFolder}/[name].css`,
        }),
        new Dotenv({
            path: './.env-prod',
        }),
        new CopyPlugin([
            {from: '../base/manifest.json', to: './'},
            {from: '../base/ext/', to: './js'},
            {from: '../base/background.js', to: './'},
            {from: '../base/images/', to: './images'},
        ]),
    ],
};
