/* eslint-disable */
var express = require('express');
var app = express();
var morgan = require('morgan');
var mockedData = require('./votes.json');

app.use(morgan('dev'));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST, OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-api-key");
    next();
});

app.get('/votes', function (req, res) {
    setTimeout(function () {
        res.type('application/json');
        res.send(mockedData);
    }, 2000);
});

app.post('/votes', function (req, res) {
    setTimeout(function () {
        res.type('application/json');
        res.send({status: '200', response: 'ok'});
    }, 2000);
});

app.post('/options', function (req, res) {
    setTimeout(function () {
        res.type('application/json');
        res.send({status: '200', response: 'ok'});
    }, 2000);
});

const port = 8081;
app.listen(port, function() {
    console.log('Mock server is listening on port %d', port);
});
